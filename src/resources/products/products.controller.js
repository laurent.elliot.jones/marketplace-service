'use strict';

var products = require('./products.json');

exports.index = function*(next) {
	this.status = 200;
  this.body = products;
};